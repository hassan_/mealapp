import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Platform, ImageBackground } from 'react-native';
import DefaultText from './DefaultText';

const MealItem = props => {
    return (
        <View style={styles.mealItem}>
            <TouchableOpacity onPress={props.onSelectMeal}>
                <View>
                    <View style={{ ...styles.mealRow, ...styles.mealHeader }}>
                        <ImageBackground style={styles.bdImag} source={{ uri: props.image }} >
                            <View style={styles.titleContainer}>
                            <Text style={styles.title} numberOfLines={1}>{props.title}</Text>
                            </View>
                        </ImageBackground>
                    </View>
                    <View style={{ ...styles.mealRow, ...styles.mealDetail }} >
                        <DefaultText>{props.duration}m</DefaultText>
                        <DefaultText>{props.complexity.toUpperCase()}</DefaultText>
                        <DefaultText>{props.affordability.toUpperCase()}</DefaultText>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    mealItem: {
        marginTop:10,
        height: 200,
        width: '100%',
        backgroundColor: '#ccc',
        overflow: Platform.OS === 'android' && Platform.Version >= 21 ? 'hidden' : 'visible',
        borderRadius:7,
    },
    bdImag: {
        width: '100%',
        height: '100%',
        justifyContent:'flex-end'
    },
    mealRow: {
        flexDirection: 'row'
    },
    mealHeader: {
        height: '85%'
    },
    mealDetail: {
        paddingHorizontal:10,
        justifyContent: 'space-between',
        alignItems:'center',
        height: '15%',
        
    },
    titleContainer:{
        backgroundColor: 'rgba(0,0,0,0.4)',
        paddingHorizontal:12,
        paddingVertical:5,
    },
    title:{
        fontFamily: 'open-sans-Bold',
        fontSize:20,
        color: 'white',
        textAlign:'center',
    },
  

});
export default MealItem;