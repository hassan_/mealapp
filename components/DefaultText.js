import React from 'react';
import { View, Text, StyleSheet, } from 'react-native';

const DefaultText = props => {
 return <Text style={styles.text}>{props.children}</Text>
};


const styles = StyleSheet.create({
    text:{
        fontFamily: 'open-sans-Regular',
        fontSize:16,
        color: 'black',
        textAlign:'left',
    }
});

export default DefaultText;