import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MealsNavigator from './navigation/MealsNavigator';
import { useScreens } from 'react-native-screens';
// Commands npm install --save expo-font  or expo install --save expo-font
import * as Font from 'expo-font';
import { AppLoading } from 'expo';

//redux
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import mealsReducer from './store/reducers/meals';


//useScreens();
const rootReducer = combineReducers({
  meals: mealsReducer,
});
const store = createStore(rootReducer);


const fetchFonts = () => {
  return Font.loadAsync({
    'open-sans-Regular': require('./assets/fonts/ProductSansRegular.ttf'),
    'open-sans-Bold': require('./assets/fonts/ProductSansBold.ttf'),
    'open-sans-BoldItalic': require('./assets/fonts/ProductSansBoldItalic.ttf'),
    'open-sans-Italic': require('./assets/fonts/ProductSansItalic.ttf'),
  });
};

export default function App() {
  const [fontLoaded, setFontoaded] = useState(false);
  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setFontoaded(true)}
        onError={(err) => console.log(err)}
      />
    )
  }

  return (
    <Provider store={store}>
      <MealsNavigator />
    </Provider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});



