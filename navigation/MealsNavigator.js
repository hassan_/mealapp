import React from 'react';
import { Platform, Text } from 'react-native';

import { createAppContainer } from "react-navigation";
import HeaderTitle, { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { Ionicons } from '@expo/vector-icons';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

import Colors from '../constants/Colors';
import CategoriesScreen from "../screens/CategoriesScreen";
import CategoryMealsScreen from '../screens/CategoryMealsScreen';
import MealDetailScreen from '../screens/MealDetailScreen';
import FavoritesScreen from '../screens/FavoritesScreen';
import FiltersScreen from '../screens/FiltersScreen';

const defaultStackNavOptions = {
    //mode: 'modal', 'card'
    //initialRouteName: 'MealDetail', //default route set here
    headerStyle: {
        backgroundColor: Platform.OS === 'android' ? Colors.primaryColor : ''
    },
    headerTitleStyle:{
        fontFamily:'open-sans-Bold'
    },
    headerBackTitleStyle:{
        fontFamily:'open-sans-Regular' //used for ios
    },
    headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primaryColor,
    headerTitle: 'Default Screen'
}

const MealsNavigator = createStackNavigator({
    Categories: {
        screen: CategoriesScreen,
        // navigationOptions:{
        //     headerTitle: 'Meal Categories!!!!'
        // } higher important
    },
    CategoryMeals: { screen: CategoryMealsScreen },
    MealDetail: MealDetailScreen,
}, {
        defaultNavigationOptions: defaultStackNavOptions
    });

const FavNavigator = createStackNavigator({
    Favorites: FavoritesScreen,
    MealDetail: MealDetailScreen
}, {
        defaultNavigationOptions: defaultStackNavOptions
    });

const tabScreenConfig = {
    Meals: {
        screen: MealsNavigator,
        navigationOptions: {
            tabBarLable: 'Meals!!!!',
            tabBarIcon: (tabInfo) => {
                return <Ionicons name='ios-restaurant' size={25} color={tabInfo.tintColor} />
            },
            tabBarColor: Colors.primaryColor,
            tabBarLabel: Platform.OS == 'android' ? <Text style={{fontFamily: 'open-sans-Bold'}}>MEAL!!!</Text> : 'MEAL',
        }
    },
    Favorites: {
        screen: FavNavigator,
        navigationOptions: {
            tabBarLable: 'Favorites!',
            tabBarIcon: (tabInfo) => {
                return <Ionicons name='ios-star' size={25} color={tabInfo.tintColor} />
            },
            tabBarColor: Colors.accentColor,
            tabBarLabel: Platform.OS == 'android' ? <Text style={{fontFamily: 'open-sans-Bold'}}>FAVORITES!!!</Text> : 'FAVORITES',
        }
    }
};


const MealsFavTabNavigation =
    Platform.OS === 'android'
        ? createMaterialBottomTabNavigator(tabScreenConfig, {
            activeTintColor: Colors.accentColor,
            shifting: true,
            // barStyle:{
            //   backgroundColor: Colors.primaryColor
            // }
        })
        : createBottomTabNavigator(tabScreenConfig, {
            tabBarOptions: {
                labelStyle:{
                    fontFamily: 'open-sans-Bold' 
                },
                activeTintColor: Colors.accentColor
            }
        });


const FiltersNavigator = createStackNavigator({
    Filters: FiltersScreen
}, 
{
    navigationOptions:{
     drawerLablel: 'Filters!!'
    },
    defaultNavigationOptions: defaultStackNavOptions
});

const MainNavigator = createDrawerNavigator({
    MealsFav:{
        screen: MealsFavTabNavigation,
        navigationOptions:{
           drawerLabel:'Meals' 
        } 
    },
    Filters: FiltersNavigator
},
{
    contentOptions:{
        activeTintColor: Colors.accentColor,
        labelStyle:{
            //marginVertical:50,
            fontFamily: 'open-sans-Bold',
            //fontSize:22
        }
    }
});

export default createAppContainer(MainNavigator);