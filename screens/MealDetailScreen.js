import React,{useEffect, useCallback } from 'react';
import {View, Text, StyleSheet , ScrollView, Image} from 'react-native';
import HeaderButton from '../components/HeaderButton';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import DefaultText from '../components/DefaultText';
import {useSelector, useDispatch } from 'react-redux';
import { toggleFavorite } from '../store/actions/meal';



const ListItem = props => {
    return(
        <View style={styles.listItem}>
        <DefaultText>{props.children}</DefaultText>
        </View>
    )
}

const MealDetailScreen = props => {
    const availableMeals = useSelector(state => state.meals.meals);
    const mealId = props.navigation.getParam('mealId');
    const currentMealIsFavorite = useSelector(state =>
         state.meals.favoriteMeals.some(meal => meal.id === mealId)
        );

    useEffect(() => {
            props.navigation.setParams({isFav: currentMealIsFavorite});
        }, [currentMealIsFavorite]);
    
    const selectedMeal = availableMeals.find((meal) => meal.id === mealId);
    const dispatch = useDispatch();
    const toggleFavoriteHandler = useCallback(() => {
       dispatch(toggleFavorite(mealId));
    });
    useEffect(() => {
       // props.navigation.setParams({mealTitle: selectedMeal.title});
       props.navigation.setParams({toggleFav: toggleFavoriteHandler });
    }, [dispatch, mealId]);

    
    

    return (
    <ScrollView> 
     <Image source={{uri: selectedMeal.imageUrl}} style={styles.image} />
     <View style={styles.detail} >
        <DefaultText>{selectedMeal.duration}m</DefaultText>
        <DefaultText>{selectedMeal.complexity.toUpperCase()}</DefaultText>
        <DefaultText>{selectedMeal.affordability.toUpperCase()}</DefaultText>
    </View>   
    <Text style={styles.title}>INGREDIENTS</Text>
     {selectedMeal.ingradients.map( (ingradient) => (
      <ListItem key={ingradient}>{ingradient}</ListItem>
     ))}
    <Text style={styles.title}>STEPS</Text>
    {selectedMeal.steps.map( (step) => (
      <ListItem key={step}>{step}</ListItem>
     ))}
    </ScrollView>
    );
};

MealDetailScreen.navigationOptions = (navigationData) => {
    //const selectedMeal = MEALS.find((meal) => meal.id === mealId);
    //const mealId = navigationData.navigation.getParam('mealId');
    const mealTitle = navigationData.navigation.getParam('mealTitle');
    const toggleFavorite = navigationData.navigation.getParam('toggleFav');
    const isFavorite = navigationData.navigation.getParam('isFav');
    
    return {
        headerTitle: mealTitle,
        headerRight: (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item 
            title="Favorite"
            iconName={isFavorite ? 'ios-star' : 'ios-star-outline'}
            onPress={toggleFavorite}
            />
            </HeaderButtons>
        ) 
    }
    
}


const styles = StyleSheet.create({
image:{
    width: '100%',
    height:200,
},
detail:{
    flexDirection: 'row',
    padding:15,
    justifyContent:'space-around'
},
title:{
    fontFamily:'open-sans-Bold',
    fontSize:22,
    textAlign: 'center'
},
listItem:{
    marginVertical:10,
    marginHorizontal:20,
    borderColor: '#ccc',
    borderWidth:1,
    padding:10,
    borderRadius:8
}
});

export default MealDetailScreen;